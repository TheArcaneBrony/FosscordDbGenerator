﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Application
    {
        public Application()
        {
            Messages = new HashSet<Message>();
            Webhooks = new HashSet<Webhook>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public string RpcOrigins { get; set; }
        public byte[] BotPublic { get; set; }
        public byte[] BotRequireCodeGrant { get; set; }
        public string TermsOfServiceUrl { get; set; }
        public string PrivacyPolicyUrl { get; set; }
        public string Summary { get; set; }
        public string VerifyKey { get; set; }
        public string PrimarySkuId { get; set; }
        public string Slug { get; set; }
        public string CoverImage { get; set; }
        public string Flags { get; set; }
        public string OwnerId { get; set; }
        public string TeamId { get; set; }
        public string GuildId { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual User Owner { get; set; }
        public virtual Team Team { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Webhook> Webhooks { get; set; }
    }
}
