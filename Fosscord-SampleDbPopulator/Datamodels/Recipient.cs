﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Recipient
    {
        public string Id { get; set; }
        public string ChannelId { get; set; }
        public string UserId { get; set; }
        public byte[] Closed { get; set; }

        public virtual Channel Channel { get; set; }
        public virtual User User { get; set; }
    }
}
