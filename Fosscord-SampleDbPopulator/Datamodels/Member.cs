﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Member
    {
        public Member()
        {
            MemberRoles = new HashSet<MemberRole>();
            Messages = new HashSet<Message>();
        }

        public long Index { get; set; }
        public string Id { get; set; }
        public string GuildId { get; set; }
        public string Nick { get; set; }
        public byte[] JoinedAt { get; set; }
        public long? PremiumSince { get; set; }
        public byte[] Deaf { get; set; }
        public byte[] Mute { get; set; }
        public byte[] Pending { get; set; }
        public string Settings { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual User IdNavigation { get; set; }
        public virtual ICollection<MemberRole> MemberRoles { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
    }
}
