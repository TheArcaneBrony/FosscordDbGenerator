﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Message
    {
        public Message()
        {
            Attachments = new HashSet<Attachment>();
            InverseMessageReferenceNavigation = new HashSet<Message>();
            MessageChannelMentions = new HashSet<MessageChannelMention>();
            MessageRoleMentions = new HashSet<MessageRoleMention>();
            MessageStickers = new HashSet<MessageSticker>();
            MessageUserMentions = new HashSet<MessageUserMention>();
            ReadStates = new HashSet<ReadState>();
        }

        public string Id { get; set; }
        public string ChannelId { get; set; }
        public string GuildId { get; set; }
        public string AuthorId { get; set; }
        public long? MemberId { get; set; }
        public string WebhookId { get; set; }
        public string ApplicationId { get; set; }
        public string Content { get; set; }
        public byte[] Timestamp { get; set; }
        public byte[] EditedTimestamp { get; set; }
        public byte[] Tts { get; set; }
        public byte[] MentionEveryone { get; set; }
        public string Embeds { get; set; }
        public string Reactions { get; set; }
        public string Nonce { get; set; }
        public byte[] Pinned { get; set; }
        public string Type { get; set; }
        public string Activity { get; set; }
        public string Flags { get; set; }
        public string MessageReference { get; set; }
        public string Interaction { get; set; }
        public string Components { get; set; }
        public string MessageReferenceId { get; set; }

        public virtual Application Application { get; set; }
        public virtual User Author { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual Member Member { get; set; }
        public virtual Message MessageReferenceNavigation { get; set; }
        public virtual Webhook Webhook { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<Message> InverseMessageReferenceNavigation { get; set; }
        public virtual ICollection<MessageChannelMention> MessageChannelMentions { get; set; }
        public virtual ICollection<MessageRoleMention> MessageRoleMentions { get; set; }
        public virtual ICollection<MessageSticker> MessageStickers { get; set; }
        public virtual ICollection<MessageUserMention> MessageUserMentions { get; set; }
        public virtual ICollection<ReadState> ReadStates { get; set; }
    }
}
