﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class ConnectedAccount
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string AccessToken { get; set; }
        public byte[] FriendSync { get; set; }
        public string Name { get; set; }
        public byte[] Revoked { get; set; }
        public byte[] ShowActivity { get; set; }
        public string Type { get; set; }
        public byte[] Verifie { get; set; }
        public long Visibility { get; set; }

        public virtual User User { get; set; }
    }
}
