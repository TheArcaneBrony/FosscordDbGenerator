﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class ReadState
    {
        public string Id { get; set; }
        public string ChannelId { get; set; }
        public string UserId { get; set; }
        public string LastMessageId { get; set; }
        public byte[] LastPinTimestamp { get; set; }
        public long MentionCount { get; set; }
        public byte[] Manual { get; set; }

        public virtual Channel Channel { get; set; }
        public virtual Message LastMessage { get; set; }
        public virtual User User { get; set; }
    }
}
