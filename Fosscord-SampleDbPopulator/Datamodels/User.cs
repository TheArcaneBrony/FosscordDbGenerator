﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class User
    {
        public User()
        {
            Applications = new HashSet<Application>();
            AuditLogTargets = new HashSet<AuditLog>();
            AuditLogUsers = new HashSet<AuditLog>();
            BanExecutors = new HashSet<Ban>();
            BanUsers = new HashSet<Ban>();
            Channels = new HashSet<Channel>();
            ConnectedAccounts = new HashSet<ConnectedAccount>();
            Guilds = new HashSet<Guild>();
            InviteInviters = new HashSet<Invite>();
            InviteTargetUsers = new HashSet<Invite>();
            Members = new HashSet<Member>();
            MessageUserMentions = new HashSet<MessageUserMention>();
            Messages = new HashSet<Message>();
            ReadStates = new HashSet<ReadState>();
            Recipients = new HashSet<Recipient>();
            RelationshipFroms = new HashSet<Relationship>();
            RelationshipTos = new HashSet<Relationship>();
            Sessions = new HashSet<Session>();
            TeamMembers = new HashSet<TeamMember>();
            Teams = new HashSet<Team>();
            Templates = new HashSet<Template>();
            VoiceStates = new HashSet<VoiceState>();
            Webhooks = new HashSet<Webhook>();
        }

        public string Id { get; set; }
        public string Username { get; set; }
        public string Discriminator { get; set; }
        public string Avatar { get; set; }
        public long? AccentColor { get; set; }
        public string Banner { get; set; }
        public string Phone { get; set; }
        public byte[] Desktop { get; set; }
        public byte[] Mobile { get; set; }
        public byte[] Premium { get; set; }
        public long PremiumType { get; set; }
        public byte[] Bot { get; set; }
        public string Bio { get; set; }
        public byte[] System { get; set; }
        public byte[] NsfwAllowed { get; set; }
        public byte[] MfaEnabled { get; set; }
        public byte[] CreatedAt { get; set; }
        public byte[] Verified { get; set; }
        public byte[] Disabled { get; set; }
        public byte[] Deleted { get; set; }
        public string Email { get; set; }
        public string Flags { get; set; }
        public long PublicFlags { get; set; }
        public string Rights { get; set; }
        public string Data { get; set; }
        public string Fingerprints { get; set; }
        public string Settings { get; set; }

        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<AuditLog> AuditLogTargets { get; set; }
        public virtual ICollection<AuditLog> AuditLogUsers { get; set; }
        public virtual ICollection<Ban> BanExecutors { get; set; }
        public virtual ICollection<Ban> BanUsers { get; set; }
        public virtual ICollection<Channel> Channels { get; set; }
        public virtual ICollection<ConnectedAccount> ConnectedAccounts { get; set; }
        public virtual ICollection<Guild> Guilds { get; set; }
        public virtual ICollection<Invite> InviteInviters { get; set; }
        public virtual ICollection<Invite> InviteTargetUsers { get; set; }
        public virtual ICollection<Member> Members { get; set; }
        public virtual ICollection<MessageUserMention> MessageUserMentions { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<ReadState> ReadStates { get; set; }
        public virtual ICollection<Recipient> Recipients { get; set; }
        public virtual ICollection<Relationship> RelationshipFroms { get; set; }
        public virtual ICollection<Relationship> RelationshipTos { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }
        public virtual ICollection<TeamMember> TeamMembers { get; set; }
        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<Template> Templates { get; set; }
        public virtual ICollection<VoiceState> VoiceStates { get; set; }
        public virtual ICollection<Webhook> Webhooks { get; set; }
    }
}
