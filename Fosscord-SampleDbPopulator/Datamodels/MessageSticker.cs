﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class MessageSticker
    {
        public string MessagesId { get; set; }
        public string StickersId { get; set; }

        public virtual Message Messages { get; set; }
        public virtual Sticker Stickers { get; set; }
    }
}
