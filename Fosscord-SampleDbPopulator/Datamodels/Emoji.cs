﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Emoji
    {
        public string Id { get; set; }
        public byte[] Animated { get; set; }
        public byte[] Available { get; set; }
        public string GuildId { get; set; }
        public byte[] Managed { get; set; }
        public string Name { get; set; }
        public byte[] RequireColons { get; set; }

        public virtual Guild Guild { get; set; }
    }
}
