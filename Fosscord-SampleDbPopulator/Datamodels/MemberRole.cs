﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class MemberRole
    {
        public long Index { get; set; }
        public string RoleId { get; set; }

        public virtual Member IndexNavigation { get; set; }
        public virtual Role Role { get; set; }
    }
}
