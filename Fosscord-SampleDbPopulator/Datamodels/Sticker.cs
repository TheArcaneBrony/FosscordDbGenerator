﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Sticker
    {
        public Sticker()
        {
            MessageStickers = new HashSet<MessageSticker>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public string PackId { get; set; }
        public string GuildId { get; set; }
        public string Type { get; set; }
        public string FormatType { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual ICollection<MessageSticker> MessageStickers { get; set; }
    }
}
