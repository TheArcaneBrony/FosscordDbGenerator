﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Config
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
