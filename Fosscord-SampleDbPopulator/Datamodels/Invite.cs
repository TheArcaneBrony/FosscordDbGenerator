﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Invite
    {
        public Invite()
        {
            Guilds = new HashSet<Guild>();
        }

        public string Code { get; set; }
        public byte[] Temporary { get; set; }
        public long Uses { get; set; }
        public long MaxUses { get; set; }
        public long MaxAge { get; set; }
        public byte[] CreatedAt { get; set; }
        public byte[] ExpiresAt { get; set; }
        public string GuildId { get; set; }
        public string ChannelId { get; set; }
        public string InviterId { get; set; }
        public string TargetUserId { get; set; }
        public long? TargetUserType { get; set; }

        public virtual Channel Channel { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual User Inviter { get; set; }
        public virtual User TargetUser { get; set; }
        public virtual ICollection<Guild> Guilds { get; set; }
    }
}
