﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class RateLimit
    {
        public string Id { get; set; }
        public string ExecutorId { get; set; }
        public long Hits { get; set; }
        public byte[] Blocked { get; set; }
        public byte[] ExpiresAt { get; set; }
    }
}
