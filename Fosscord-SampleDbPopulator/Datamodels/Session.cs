﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Session
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string SessionId { get; set; }
        public string ClientInfo { get; set; }
        public string Status { get; set; }

        public virtual User User { get; set; }
    }
}
