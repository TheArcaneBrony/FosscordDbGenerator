﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class QueryResultCache
    {
        public long Id { get; set; }
        public string Identifier { get; set; }
        public long Time { get; set; }
        public long Duration { get; set; }
        public string Query { get; set; }
        public string Result { get; set; }
    }
}
