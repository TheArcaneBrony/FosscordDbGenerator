﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Ban
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string GuildId { get; set; }
        public string ExecutorId { get; set; }
        public string Ip { get; set; }
        public string Reason { get; set; }

        public virtual User Executor { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual User User { get; set; }
    }
}
