﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class MessageChannelMention
    {
        public string MessagesId { get; set; }
        public string ChannelsId { get; set; }

        public virtual Channel Channels { get; set; }
        public virtual Message Messages { get; set; }
    }
}
