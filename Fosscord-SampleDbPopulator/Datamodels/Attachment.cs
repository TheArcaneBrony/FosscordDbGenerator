﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Attachment
    {
        public string Id { get; set; }
        public string Filename { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
        public string ProxyUrl { get; set; }
        public long? Height { get; set; }
        public long? Width { get; set; }
        public string ContentType { get; set; }
        public string MessageId { get; set; }

        public virtual Message Message { get; set; }
    }
}
