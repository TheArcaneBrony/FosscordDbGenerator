﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Guild
    {
        public Guild()
        {
            Applications = new HashSet<Application>();
            Bans = new HashSet<Ban>();
            Channels = new HashSet<Channel>();
            Emojis = new HashSet<Emoji>();
            Invites = new HashSet<Invite>();
            Members = new HashSet<Member>();
            Messages = new HashSet<Message>();
            Roles = new HashSet<Role>();
            Stickers = new HashSet<Sticker>();
            Templates = new HashSet<Template>();
            VoiceStates = new HashSet<VoiceState>();
            WebhookGuilds = new HashSet<Webhook>();
            WebhookSourceGuilds = new HashSet<Webhook>();
        }

        public string Id { get; set; }
        public string AfkChannelId { get; set; }
        public long? AfkTimeout { get; set; }
        public string Banner { get; set; }
        public long? DefaultMessageNotifications { get; set; }
        public string Description { get; set; }
        public string DiscoverySplash { get; set; }
        public long? ExplicitContentFilter { get; set; }
        public string Features { get; set; }
        public string Icon { get; set; }
        public byte[] Large { get; set; }
        public long? MaxMembers { get; set; }
        public long? MaxPresences { get; set; }
        public long? MaxVideoChannelUsers { get; set; }
        public long? MemberCount { get; set; }
        public long? PresenceCount { get; set; }
        public string TemplateId { get; set; }
        public long? MfaLevel { get; set; }
        public string Name { get; set; }
        public string OwnerId { get; set; }
        public string PreferredLocale { get; set; }
        public long? PremiumSubscriptionCount { get; set; }
        public long? PremiumTier { get; set; }
        public string PublicUpdatesChannelId { get; set; }
        public string RulesChannelId { get; set; }
        public string Region { get; set; }
        public string Splash { get; set; }
        public string SystemChannelId { get; set; }
        public long? SystemChannelFlags { get; set; }
        public byte[] Unavailable { get; set; }
        public long? VerificationLevel { get; set; }
        public string WelcomeScreen { get; set; }
        public string WidgetChannelId { get; set; }
        public byte[] WidgetEnabled { get; set; }
        public long? NsfwLevel { get; set; }
        public byte[] Nsfw { get; set; }
        public string VanityUrlCode { get; set; }

        public virtual Channel AfkChannel { get; set; }
        public virtual User Owner { get; set; }
        public virtual Channel PublicUpdatesChannel { get; set; }
        public virtual Channel RulesChannel { get; set; }
        public virtual Channel SystemChannel { get; set; }
        public virtual Template Template { get; set; }
        public virtual Invite VanityUrlCodeNavigation { get; set; }
        public virtual Channel WidgetChannel { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<Ban> Bans { get; set; }
        public virtual ICollection<Channel> Channels { get; set; }
        public virtual ICollection<Emoji> Emojis { get; set; }
        public virtual ICollection<Invite> Invites { get; set; }
        public virtual ICollection<Member> Members { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Sticker> Stickers { get; set; }
        public virtual ICollection<Template> Templates { get; set; }
        public virtual ICollection<VoiceState> VoiceStates { get; set; }
        public virtual ICollection<Webhook> WebhookGuilds { get; set; }
        public virtual ICollection<Webhook> WebhookSourceGuilds { get; set; }
    }
}
