﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Relationship
    {
        public string Id { get; set; }
        public string FromId { get; set; }
        public string ToId { get; set; }
        public string Nickname { get; set; }
        public string Type { get; set; }

        public virtual User From { get; set; }
        public virtual User To { get; set; }
    }
}
