﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Db : DbContext
    {
        public Db()
        {
        }

        public Db(DbContextOptions<Db> options)
            : base(options)
        {
        }

        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<AuditLog> AuditLogs { get; set; }
        public virtual DbSet<Ban> Bans { get; set; }
        public virtual DbSet<Channel> Channels { get; set; }
        public virtual DbSet<Config> Configs { get; set; }
        public virtual DbSet<ConnectedAccount> ConnectedAccounts { get; set; }
        public virtual DbSet<Emoji> Emojis { get; set; }
        public virtual DbSet<Guild> Guilds { get; set; }
        public virtual DbSet<Invite> Invites { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<MemberRole> MemberRoles { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<MessageChannelMention> MessageChannelMentions { get; set; }
        public virtual DbSet<MessageRoleMention> MessageRoleMentions { get; set; }
        public virtual DbSet<MessageSticker> MessageStickers { get; set; }
        public virtual DbSet<MessageUserMention> MessageUserMentions { get; set; }
        public virtual DbSet<QueryResultCache> QueryResultCaches { get; set; }
        public virtual DbSet<RateLimit> RateLimits { get; set; }
        public virtual DbSet<ReadState> ReadStates { get; set; }
        public virtual DbSet<Recipient> Recipients { get; set; }
        public virtual DbSet<Relationship> Relationships { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }
        public virtual DbSet<Sticker> Stickers { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<TeamMember> TeamMembers { get; set; }
        public virtual DbSet<Template> Templates { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<VoiceState> VoiceStates { get; set; }
        public virtual DbSet<Webhook> Webhooks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlite("Data Source=C:\\Users\\TheArcaneBrony\\Desktop\\fosscord-arcane\\bundle\\database.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Application>(entity =>
            {
                entity.ToTable("applications");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.BotPublic)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("bot_public");

                entity.Property(e => e.BotRequireCodeGrant)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("bot_require_code_grant");

                entity.Property(e => e.CoverImage)
                    .HasColumnType("varchar")
                    .HasColumnName("cover_image");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("description");

                entity.Property(e => e.Flags)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("flags");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Icon)
                    .HasColumnType("varchar")
                    .HasColumnName("icon");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.OwnerId)
                    .HasColumnType("varchar")
                    .HasColumnName("owner_id");

                entity.Property(e => e.PrimarySkuId)
                    .HasColumnType("varchar")
                    .HasColumnName("primary_sku_id");

                entity.Property(e => e.PrivacyPolicyUrl)
                    .HasColumnType("varchar")
                    .HasColumnName("privacy_policy_url");

                entity.Property(e => e.RpcOrigins)
                    .HasColumnType("text")
                    .HasColumnName("rpc_origins");

                entity.Property(e => e.Slug)
                    .HasColumnType("varchar")
                    .HasColumnName("slug");

                entity.Property(e => e.Summary)
                    .HasColumnType("varchar")
                    .HasColumnName("summary");

                entity.Property(e => e.TeamId)
                    .HasColumnType("varchar")
                    .HasColumnName("team_id");

                entity.Property(e => e.TermsOfServiceUrl)
                    .HasColumnType("varchar")
                    .HasColumnName("terms_of_service_url");

                entity.Property(e => e.VerifyKey)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("verify_key");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Applications)
                    .HasForeignKey(d => d.GuildId);

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Applications)
                    .HasForeignKey(d => d.OwnerId);

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.Applications)
                    .HasForeignKey(d => d.TeamId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Attachment>(entity =>
            {
                entity.ToTable("attachments");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ContentType)
                    .HasColumnType("varchar")
                    .HasColumnName("content_type");

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("filename");

                entity.Property(e => e.Height)
                    .HasColumnType("integer")
                    .HasColumnName("height");

                entity.Property(e => e.MessageId)
                    .HasColumnType("varchar")
                    .HasColumnName("message_id");

                entity.Property(e => e.ProxyUrl)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("proxy_url");

                entity.Property(e => e.Size)
                    .HasColumnType("integer")
                    .HasColumnName("size");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("url");

                entity.Property(e => e.Width)
                    .HasColumnType("integer")
                    .HasColumnName("width");

                entity.HasOne(d => d.Message)
                    .WithMany(p => p.Attachments)
                    .HasForeignKey(d => d.MessageId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<AuditLog>(entity =>
            {
                entity.ToTable("audit_logs");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ActionType)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("action_type");

                entity.Property(e => e.Changes)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("changes");

                entity.Property(e => e.Options)
                    .HasColumnType("text")
                    .HasColumnName("options");

                entity.Property(e => e.Reason)
                    .HasColumnType("varchar")
                    .HasColumnName("reason");

                entity.Property(e => e.TargetId)
                    .HasColumnType("varchar")
                    .HasColumnName("target_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.Target)
                    .WithMany(p => p.AuditLogTargets)
                    .HasForeignKey(d => d.TargetId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AuditLogUsers)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Ban>(entity =>
            {
                entity.ToTable("bans");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ExecutorId)
                    .HasColumnType("varchar")
                    .HasColumnName("executor_id");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Ip)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("ip");

                entity.Property(e => e.Reason)
                    .HasColumnType("varchar")
                    .HasColumnName("reason");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.Executor)
                    .WithMany(p => p.BanExecutors)
                    .HasForeignKey(d => d.ExecutorId);

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Bans)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.BanUsers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Channel>(entity =>
            {
                entity.ToTable("channels");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Bitrate)
                    .HasColumnType("integer")
                    .HasColumnName("bitrate");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.Property(e => e.DefaultAutoArchiveDuration)
                    .HasColumnType("integer")
                    .HasColumnName("default_auto_archive_duration");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Icon)
                    .HasColumnType("text")
                    .HasColumnName("icon");

                entity.Property(e => e.LastMessageId)
                    .HasColumnType("varchar")
                    .HasColumnName("last_message_id");

                entity.Property(e => e.LastPinTimestamp)
                    .HasColumnType("integer")
                    .HasColumnName("last_pin_timestamp");

                entity.Property(e => e.Name)
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.Nsfw)
                    .HasColumnType("boolean")
                    .HasColumnName("nsfw");

                entity.Property(e => e.OwnerId)
                    .HasColumnType("varchar")
                    .HasColumnName("owner_id");

                entity.Property(e => e.ParentId)
                    .HasColumnType("varchar")
                    .HasColumnName("parent_id");

                entity.Property(e => e.PermissionOverwrites)
                    .HasColumnType("text")
                    .HasColumnName("permission_overwrites");

                entity.Property(e => e.Position)
                    .HasColumnType("integer")
                    .HasColumnName("position");

                entity.Property(e => e.RateLimitPerUser)
                    .HasColumnType("integer")
                    .HasColumnName("rate_limit_per_user");

                entity.Property(e => e.Topic)
                    .HasColumnType("varchar")
                    .HasColumnName("topic");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("type");

                entity.Property(e => e.UserLimit)
                    .HasColumnType("integer")
                    .HasColumnName("user_limit");

                entity.Property(e => e.VideoQualityMode)
                    .HasColumnType("integer")
                    .HasColumnName("video_quality_mode");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Channels)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Channels)
                    .HasForeignKey(d => d.OwnerId);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId);
            });

            modelBuilder.Entity<Config>(entity =>
            {
                entity.HasKey(e => e.Key);

                entity.ToTable("config");

                entity.Property(e => e.Key)
                    .HasColumnType("varchar")
                    .HasColumnName("key");

                entity.Property(e => e.Value)
                    .HasColumnType("text")
                    .HasColumnName("value");
            });

            modelBuilder.Entity<ConnectedAccount>(entity =>
            {
                entity.ToTable("connected_accounts");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.AccessToken)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("access_token");

                entity.Property(e => e.FriendSync)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("friend_sync");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.Revoked)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("revoked");

                entity.Property(e => e.ShowActivity)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("show_activity");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("type");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.Property(e => e.Verifie)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("verifie");

                entity.Property(e => e.Visibility)
                    .HasColumnType("integer")
                    .HasColumnName("visibility");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ConnectedAccounts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Emoji>(entity =>
            {
                entity.ToTable("emojis");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Animated)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("animated");

                entity.Property(e => e.Available)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("available");

                entity.Property(e => e.GuildId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Managed)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("managed");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.RequireColons)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("require_colons");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Emojis)
                    .HasForeignKey(d => d.GuildId);
            });

            modelBuilder.Entity<Guild>(entity =>
            {
                entity.ToTable("guilds");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.AfkChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("afk_channel_id");

                entity.Property(e => e.AfkTimeout)
                    .HasColumnType("integer")
                    .HasColumnName("afk_timeout");

                entity.Property(e => e.Banner)
                    .HasColumnType("varchar")
                    .HasColumnName("banner");

                entity.Property(e => e.DefaultMessageNotifications)
                    .HasColumnType("integer")
                    .HasColumnName("default_message_notifications");

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasColumnName("description");

                entity.Property(e => e.DiscoverySplash)
                    .HasColumnType("varchar")
                    .HasColumnName("discovery_splash");

                entity.Property(e => e.ExplicitContentFilter)
                    .HasColumnType("integer")
                    .HasColumnName("explicit_content_filter");

                entity.Property(e => e.Features)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("features");

                entity.Property(e => e.Icon)
                    .HasColumnType("varchar")
                    .HasColumnName("icon");

                entity.Property(e => e.Large)
                    .HasColumnType("boolean")
                    .HasColumnName("large");

                entity.Property(e => e.MaxMembers)
                    .HasColumnType("integer")
                    .HasColumnName("max_members");

                entity.Property(e => e.MaxPresences)
                    .HasColumnType("integer")
                    .HasColumnName("max_presences");

                entity.Property(e => e.MaxVideoChannelUsers)
                    .HasColumnType("integer")
                    .HasColumnName("max_video_channel_users");

                entity.Property(e => e.MemberCount)
                    .HasColumnType("integer")
                    .HasColumnName("member_count");

                entity.Property(e => e.MfaLevel)
                    .HasColumnType("integer")
                    .HasColumnName("mfa_level");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.Nsfw)
                    .HasColumnType("boolean")
                    .HasColumnName("nsfw");

                entity.Property(e => e.NsfwLevel)
                    .HasColumnType("integer")
                    .HasColumnName("nsfw_level");

                entity.Property(e => e.OwnerId)
                    .HasColumnType("varchar")
                    .HasColumnName("owner_id");

                entity.Property(e => e.PreferredLocale)
                    .HasColumnType("varchar")
                    .HasColumnName("preferred_locale");

                entity.Property(e => e.PremiumSubscriptionCount)
                    .HasColumnType("integer")
                    .HasColumnName("premium_subscription_count");

                entity.Property(e => e.PremiumTier)
                    .HasColumnType("integer")
                    .HasColumnName("premium_tier");

                entity.Property(e => e.PresenceCount)
                    .HasColumnType("integer")
                    .HasColumnName("presence_count");

                entity.Property(e => e.PublicUpdatesChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("public_updates_channel_id");

                entity.Property(e => e.Region)
                    .HasColumnType("varchar")
                    .HasColumnName("region");

                entity.Property(e => e.RulesChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("rules_channel_id");

                entity.Property(e => e.Splash)
                    .HasColumnType("varchar")
                    .HasColumnName("splash");

                entity.Property(e => e.SystemChannelFlags)
                    .HasColumnType("integer")
                    .HasColumnName("system_channel_flags");

                entity.Property(e => e.SystemChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("system_channel_id");

                entity.Property(e => e.TemplateId)
                    .HasColumnType("varchar")
                    .HasColumnName("template_id");

                entity.Property(e => e.Unavailable)
                    .HasColumnType("boolean")
                    .HasColumnName("unavailable");

                entity.Property(e => e.VanityUrlCode)
                    .HasColumnType("varchar")
                    .HasColumnName("vanityUrlCode");

                entity.Property(e => e.VerificationLevel)
                    .HasColumnType("integer")
                    .HasColumnName("verification_level");

                entity.Property(e => e.WelcomeScreen)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("welcome_screen");

                entity.Property(e => e.WidgetChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("widget_channel_id");

                entity.Property(e => e.WidgetEnabled)
                    .HasColumnType("boolean")
                    .HasColumnName("widget_enabled");

                entity.HasOne(d => d.AfkChannel)
                    .WithMany(p => p.GuildAfkChannels)
                    .HasForeignKey(d => d.AfkChannelId);

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Guilds)
                    .HasForeignKey(d => d.OwnerId);

                entity.HasOne(d => d.PublicUpdatesChannel)
                    .WithMany(p => p.GuildPublicUpdatesChannels)
                    .HasForeignKey(d => d.PublicUpdatesChannelId);

                entity.HasOne(d => d.RulesChannel)
                    .WithMany(p => p.GuildRulesChannels)
                    .HasForeignKey(d => d.RulesChannelId);

                entity.HasOne(d => d.SystemChannel)
                    .WithMany(p => p.GuildSystemChannels)
                    .HasForeignKey(d => d.SystemChannelId);

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.Guilds)
                    .HasForeignKey(d => d.TemplateId);

                entity.HasOne(d => d.VanityUrlCodeNavigation)
                    .WithMany(p => p.Guilds)
                    .HasForeignKey(d => d.VanityUrlCode);

                entity.HasOne(d => d.WidgetChannel)
                    .WithMany(p => p.GuildWidgetChannels)
                    .HasForeignKey(d => d.WidgetChannelId);
            });

            modelBuilder.Entity<Invite>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.ToTable("invites");

                entity.Property(e => e.Code)
                    .HasColumnType("varchar")
                    .HasColumnName("code");

                entity.Property(e => e.ChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("channel_id");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.Property(e => e.ExpiresAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("expires_at");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.InviterId)
                    .HasColumnType("varchar")
                    .HasColumnName("inviter_id");

                entity.Property(e => e.MaxAge)
                    .HasColumnType("integer")
                    .HasColumnName("max_age");

                entity.Property(e => e.MaxUses)
                    .HasColumnType("integer")
                    .HasColumnName("max_uses");

                entity.Property(e => e.TargetUserId)
                    .HasColumnType("varchar")
                    .HasColumnName("target_user_id");

                entity.Property(e => e.TargetUserType)
                    .HasColumnType("integer")
                    .HasColumnName("target_user_type");

                entity.Property(e => e.Temporary)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("temporary");

                entity.Property(e => e.Uses)
                    .HasColumnType("integer")
                    .HasColumnName("uses");

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.Invites)
                    .HasForeignKey(d => d.ChannelId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Invites)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Inviter)
                    .WithMany(p => p.InviteInviters)
                    .HasForeignKey(d => d.InviterId);

                entity.HasOne(d => d.TargetUser)
                    .WithMany(p => p.InviteTargetUsers)
                    .HasForeignKey(d => d.TargetUserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Member>(entity =>
            {
                entity.HasKey(e => e.Index);

                entity.ToTable("members");

                entity.HasIndex(e => new { e.Id, e.GuildId }, "IDX_bb2bf9386ac443afbbbf9f12d3")
                    .IsUnique();

                entity.Property(e => e.Index)
                    .HasColumnType("integer")
                    .HasColumnName("index");

                entity.Property(e => e.Deaf)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("deaf");

                entity.Property(e => e.GuildId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.JoinedAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("joined_at");

                entity.Property(e => e.Mute)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("mute");

                entity.Property(e => e.Nick)
                    .HasColumnType("varchar")
                    .HasColumnName("nick");

                entity.Property(e => e.Pending)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("pending");

                entity.Property(e => e.PremiumSince)
                    .HasColumnType("integer")
                    .HasColumnName("premium_since");

                entity.Property(e => e.Settings)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("settings");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Members)
                    .HasForeignKey(d => d.GuildId);

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.Members)
                    .HasForeignKey(d => d.Id);
            });

            modelBuilder.Entity<MemberRole>(entity =>
            {
                entity.HasKey(e => new { e.Index, e.RoleId });

                entity.ToTable("member_roles");

                entity.HasIndex(e => e.Index, "IDX_5d7ddc8a5f9c167f548625e772");

                entity.HasIndex(e => e.RoleId, "IDX_e9080e7a7997a0170026d5139c");

                entity.Property(e => e.Index)
                    .HasColumnType("integer")
                    .HasColumnName("index");

                entity.Property(e => e.RoleId)
                    .HasColumnType("varchar")
                    .HasColumnName("role_id");

                entity.HasOne(d => d.IndexNavigation)
                    .WithMany(p => p.MemberRoles)
                    .HasForeignKey(d => d.Index);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.MemberRoles)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.ToTable("messages");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Activity)
                    .HasColumnType("text")
                    .HasColumnName("activity");

                entity.Property(e => e.ApplicationId)
                    .HasColumnType("varchar")
                    .HasColumnName("application_id");

                entity.Property(e => e.AuthorId)
                    .HasColumnType("varchar")
                    .HasColumnName("author_id");

                entity.Property(e => e.ChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("channel_id");

                entity.Property(e => e.Components)
                    .HasColumnType("text")
                    .HasColumnName("components");

                entity.Property(e => e.Content)
                    .HasColumnType("varchar")
                    .HasColumnName("content");

                entity.Property(e => e.EditedTimestamp)
                    .HasColumnType("datetime")
                    .HasColumnName("edited_timestamp");

                entity.Property(e => e.Embeds)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("embeds");

                entity.Property(e => e.Flags)
                    .HasColumnType("varchar")
                    .HasColumnName("flags");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Interaction)
                    .HasColumnType("text")
                    .HasColumnName("interaction");

                entity.Property(e => e.MemberId)
                    .HasColumnType("integer")
                    .HasColumnName("member_id");

                entity.Property(e => e.MentionEveryone)
                    .HasColumnType("boolean")
                    .HasColumnName("mention_everyone");

                entity.Property(e => e.MessageReference)
                    .HasColumnType("text")
                    .HasColumnName("message_reference");

                entity.Property(e => e.MessageReferenceId)
                    .HasColumnType("varchar")
                    .HasColumnName("message_reference_id");

                entity.Property(e => e.Nonce)
                    .HasColumnType("text")
                    .HasColumnName("nonce");

                entity.Property(e => e.Pinned)
                    .HasColumnType("boolean")
                    .HasColumnName("pinned");

                entity.Property(e => e.Reactions)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("reactions");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("timestamp")
                    .HasDefaultValueSql("datetime('now')");

                entity.Property(e => e.Tts)
                    .HasColumnType("boolean")
                    .HasColumnName("tts");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("type");

                entity.Property(e => e.WebhookId)
                    .HasColumnType("varchar")
                    .HasColumnName("webhook_id");

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.ApplicationId);

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.AuthorId);

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.ChannelId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.MemberId);

                entity.HasOne(d => d.MessageReferenceNavigation)
                    .WithMany(p => p.InverseMessageReferenceNavigation)
                    .HasForeignKey(d => d.MessageReferenceId);

                entity.HasOne(d => d.Webhook)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.WebhookId);
            });

            modelBuilder.Entity<MessageChannelMention>(entity =>
            {
                entity.HasKey(e => new { e.MessagesId, e.ChannelsId });

                entity.ToTable("message_channel_mentions");

                entity.HasIndex(e => e.MessagesId, "IDX_2a27102ecd1d81b4582a436092");

                entity.HasIndex(e => e.ChannelsId, "IDX_bdb8c09e1464cabf62105bf4b9");

                entity.Property(e => e.MessagesId)
                    .HasColumnType("varchar")
                    .HasColumnName("messagesId");

                entity.Property(e => e.ChannelsId)
                    .HasColumnType("varchar")
                    .HasColumnName("channelsId");

                entity.HasOne(d => d.Channels)
                    .WithMany(p => p.MessageChannelMentions)
                    .HasForeignKey(d => d.ChannelsId);

                entity.HasOne(d => d.Messages)
                    .WithMany(p => p.MessageChannelMentions)
                    .HasForeignKey(d => d.MessagesId);
            });

            modelBuilder.Entity<MessageRoleMention>(entity =>
            {
                entity.HasKey(e => new { e.MessagesId, e.RolesId });

                entity.ToTable("message_role_mentions");

                entity.HasIndex(e => e.RolesId, "IDX_29d63eb1a458200851bc37d074");

                entity.HasIndex(e => e.MessagesId, "IDX_a8242cf535337a490b0feaea0b");

                entity.Property(e => e.MessagesId)
                    .HasColumnType("varchar")
                    .HasColumnName("messagesId");

                entity.Property(e => e.RolesId)
                    .HasColumnType("varchar")
                    .HasColumnName("rolesId");

                entity.HasOne(d => d.Messages)
                    .WithMany(p => p.MessageRoleMentions)
                    .HasForeignKey(d => d.MessagesId);

                entity.HasOne(d => d.Roles)
                    .WithMany(p => p.MessageRoleMentions)
                    .HasForeignKey(d => d.RolesId);
            });

            modelBuilder.Entity<MessageSticker>(entity =>
            {
                entity.HasKey(e => new { e.MessagesId, e.StickersId });

                entity.ToTable("message_stickers");

                entity.HasIndex(e => e.MessagesId, "IDX_40bb6f23e7cc133292e92829d2");

                entity.HasIndex(e => e.StickersId, "IDX_e22a70819d07659c7a71c112a1");

                entity.Property(e => e.MessagesId)
                    .HasColumnType("varchar")
                    .HasColumnName("messagesId");

                entity.Property(e => e.StickersId)
                    .HasColumnType("varchar")
                    .HasColumnName("stickersId");

                entity.HasOne(d => d.Messages)
                    .WithMany(p => p.MessageStickers)
                    .HasForeignKey(d => d.MessagesId);

                entity.HasOne(d => d.Stickers)
                    .WithMany(p => p.MessageStickers)
                    .HasForeignKey(d => d.StickersId);
            });

            modelBuilder.Entity<MessageUserMention>(entity =>
            {
                entity.HasKey(e => new { e.MessagesId, e.UsersId });

                entity.ToTable("message_user_mentions");

                entity.HasIndex(e => e.MessagesId, "IDX_a343387fc560ef378760681c23");

                entity.HasIndex(e => e.UsersId, "IDX_b831eb18ceebd28976239b1e2f");

                entity.Property(e => e.MessagesId)
                    .HasColumnType("varchar")
                    .HasColumnName("messagesId");

                entity.Property(e => e.UsersId)
                    .HasColumnType("varchar")
                    .HasColumnName("usersId");

                entity.HasOne(d => d.Messages)
                    .WithMany(p => p.MessageUserMentions)
                    .HasForeignKey(d => d.MessagesId);

                entity.HasOne(d => d.Users)
                    .WithMany(p => p.MessageUserMentions)
                    .HasForeignKey(d => d.UsersId);
            });

            modelBuilder.Entity<QueryResultCache>(entity =>
            {
                entity.ToTable("query-result-cache");

                entity.Property(e => e.Id)
                    .HasColumnType("integer")
                    .HasColumnName("id");

                entity.Property(e => e.Duration)
                    .HasColumnType("integer")
                    .HasColumnName("duration");

                entity.Property(e => e.Identifier)
                    .HasColumnType("varchar")
                    .HasColumnName("identifier");

                entity.Property(e => e.Query)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("query");

                entity.Property(e => e.Result)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("result");

                entity.Property(e => e.Time)
                    .HasColumnType("bigint")
                    .HasColumnName("time");
            });

            modelBuilder.Entity<RateLimit>(entity =>
            {
                entity.ToTable("rate_limits");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Blocked)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("blocked");

                entity.Property(e => e.ExecutorId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("executor_id");

                entity.Property(e => e.ExpiresAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("expires_at");

                entity.Property(e => e.Hits)
                    .HasColumnType("integer")
                    .HasColumnName("hits");
            });

            modelBuilder.Entity<ReadState>(entity =>
            {
                entity.ToTable("read_states");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("channel_id");

                entity.Property(e => e.LastMessageId)
                    .HasColumnType("varchar")
                    .HasColumnName("last_message_id");

                entity.Property(e => e.LastPinTimestamp)
                    .HasColumnType("datetime")
                    .HasColumnName("last_pin_timestamp");

                entity.Property(e => e.Manual)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("manual");

                entity.Property(e => e.MentionCount)
                    .HasColumnType("integer")
                    .HasColumnName("mention_count");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.ReadStates)
                    .HasForeignKey(d => d.ChannelId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.LastMessage)
                    .WithMany(p => p.ReadStates)
                    .HasForeignKey(d => d.LastMessageId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ReadStates)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Recipient>(entity =>
            {
                entity.ToTable("recipients");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ChannelId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("channel_id");

                entity.Property(e => e.Closed)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("closed")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.Recipients)
                    .HasForeignKey(d => d.ChannelId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Recipients)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Relationship>(entity =>
            {
                entity.ToTable("relationships");

                entity.HasIndex(e => new { e.FromId, e.ToId }, "IDX_a0b2ff0a598df0b0d055934a17")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.FromId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("from_id");

                entity.Property(e => e.Nickname)
                    .HasColumnType("varchar")
                    .HasColumnName("nickname");

                entity.Property(e => e.ToId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("to_id");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("type");

                entity.HasOne(d => d.From)
                    .WithMany(p => p.RelationshipFroms)
                    .HasForeignKey(d => d.FromId);

                entity.HasOne(d => d.To)
                    .WithMany(p => p.RelationshipTos)
                    .HasForeignKey(d => d.ToId);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("roles");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Color)
                    .HasColumnType("integer")
                    .HasColumnName("color");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Hoist)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("hoist");

                entity.Property(e => e.Managed)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("managed");

                entity.Property(e => e.Mentionable)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("mentionable");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.Permissions)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("permissions");

                entity.Property(e => e.Position)
                    .HasColumnType("integer")
                    .HasColumnName("position");

                entity.Property(e => e.Tags)
                    .HasColumnType("text")
                    .HasColumnName("tags");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Roles)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Session>(entity =>
            {
                entity.ToTable("sessions");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ClientInfo)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("client_info");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("session_id");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("status");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Sessions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Sticker>(entity =>
            {
                entity.ToTable("stickers");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasColumnName("description");

                entity.Property(e => e.FormatType)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("format_type");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.PackId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("pack_id");

                entity.Property(e => e.Tags)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("tags");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("type");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Stickers)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.ToTable("teams");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Icon)
                    .HasColumnType("varchar")
                    .HasColumnName("icon");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.OwnerUserId)
                    .HasColumnType("varchar")
                    .HasColumnName("owner_user_id");

                entity.HasOne(d => d.OwnerUser)
                    .WithMany(p => p.Teams)
                    .HasForeignKey(d => d.OwnerUserId);
            });

            modelBuilder.Entity<TeamMember>(entity =>
            {
                entity.ToTable("team_members");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.MembershipState)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("membership_state");

                entity.Property(e => e.Permissions)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("permissions");

                entity.Property(e => e.TeamId)
                    .HasColumnType("varchar")
                    .HasColumnName("team_id");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.TeamMembers)
                    .HasForeignKey(d => d.TeamId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TeamMembers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Template>(entity =>
            {
                entity.ToTable("templates");

                entity.HasIndex(e => e.Code, "IX_templates_code")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("code");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.Property(e => e.CreatorId)
                    .HasColumnType("varchar")
                    .HasColumnName("creator_id");

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.SerializedSourceGuild)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("serialized_source_guild");

                entity.Property(e => e.SourceGuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("source_guild_id");

                entity.Property(e => e.UpdatedAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at");

                entity.Property(e => e.UsageCount)
                    .HasColumnType("integer")
                    .HasColumnName("usage_count");

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.Templates)
                    .HasForeignKey(d => d.CreatorId);

                entity.HasOne(d => d.SourceGuild)
                    .WithMany(p => p.Templates)
                    .HasForeignKey(d => d.SourceGuildId);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.AccentColor)
                    .HasColumnType("integer")
                    .HasColumnName("accent_color");

                entity.Property(e => e.Avatar)
                    .HasColumnType("varchar")
                    .HasColumnName("avatar");

                entity.Property(e => e.Banner)
                    .HasColumnType("varchar")
                    .HasColumnName("banner");

                entity.Property(e => e.Bio)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("bio");

                entity.Property(e => e.Bot)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("bot");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("data");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("deleted");

                entity.Property(e => e.Desktop)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("desktop");

                entity.Property(e => e.Disabled)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("disabled");

                entity.Property(e => e.Discriminator)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("discriminator");

                entity.Property(e => e.Email)
                    .HasColumnType("varchar")
                    .HasColumnName("email");

                entity.Property(e => e.Fingerprints)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("fingerprints");

                entity.Property(e => e.Flags)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("flags");

                entity.Property(e => e.MfaEnabled)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("mfa_enabled");

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("mobile");

                entity.Property(e => e.NsfwAllowed)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("nsfw_allowed");

                entity.Property(e => e.Phone)
                    .HasColumnType("varchar")
                    .HasColumnName("phone");

                entity.Property(e => e.Premium)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("premium");

                entity.Property(e => e.PremiumType)
                    .HasColumnType("integer")
                    .HasColumnName("premium_type");

                entity.Property(e => e.PublicFlags)
                    .HasColumnType("integer")
                    .HasColumnName("public_flags");

                entity.Property(e => e.Rights)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("rights");

                entity.Property(e => e.Settings)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("settings");

                entity.Property(e => e.System)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("system");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("username");

                entity.Property(e => e.Verified)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("verified");
            });

            modelBuilder.Entity<VoiceState>(entity =>
            {
                entity.ToTable("voice_states");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("channel_id");

                entity.Property(e => e.Deaf)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("deaf");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Mute)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("mute");

                entity.Property(e => e.RequestToSpeakTimestamp)
                    .HasColumnType("datetime")
                    .HasColumnName("request_to_speak_timestamp");

                entity.Property(e => e.SelfDeaf)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("self_deaf");

                entity.Property(e => e.SelfMute)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("self_mute");

                entity.Property(e => e.SelfStream)
                    .HasColumnType("boolean")
                    .HasColumnName("self_stream");

                entity.Property(e => e.SelfVideo)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("self_video");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("session_id");

                entity.Property(e => e.Suppress)
                    .IsRequired()
                    .HasColumnType("boolean")
                    .HasColumnName("suppress");

                entity.Property(e => e.Token)
                    .HasColumnType("varchar")
                    .HasColumnName("token");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.VoiceStates)
                    .HasForeignKey(d => d.ChannelId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.VoiceStates)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.VoiceStates)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Webhook>(entity =>
            {
                entity.ToTable("webhooks");

                entity.Property(e => e.Id)
                    .HasColumnType("varchar")
                    .HasColumnName("id");

                entity.Property(e => e.ApplicationId)
                    .HasColumnType("varchar")
                    .HasColumnName("application_id");

                entity.Property(e => e.Avatar)
                    .HasColumnType("varchar")
                    .HasColumnName("avatar");

                entity.Property(e => e.ChannelId)
                    .HasColumnType("varchar")
                    .HasColumnName("channel_id");

                entity.Property(e => e.GuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("guild_id");

                entity.Property(e => e.Name)
                    .HasColumnType("varchar")
                    .HasColumnName("name");

                entity.Property(e => e.SourceGuildId)
                    .HasColumnType("varchar")
                    .HasColumnName("source_guild_id");

                entity.Property(e => e.Token)
                    .HasColumnType("varchar")
                    .HasColumnName("token");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasColumnName("type");

                entity.Property(e => e.UserId)
                    .HasColumnType("varchar")
                    .HasColumnName("user_id");

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.Webhooks)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.Webhooks)
                    .HasForeignKey(d => d.ChannelId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.WebhookGuilds)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.SourceGuild)
                    .WithMany(p => p.WebhookSourceGuilds)
                    .HasForeignKey(d => d.SourceGuildId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Webhooks)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
