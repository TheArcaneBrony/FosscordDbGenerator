﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Team
    {
        public Team()
        {
            Applications = new HashSet<Application>();
            TeamMembers = new HashSet<TeamMember>();
        }

        public string Id { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string OwnerUserId { get; set; }

        public virtual User OwnerUser { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<TeamMember> TeamMembers { get; set; }
    }
}
