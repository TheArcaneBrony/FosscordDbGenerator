﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Template
    {
        public Template()
        {
            Guilds = new HashSet<Guild>();
        }

        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? UsageCount { get; set; }
        public string CreatorId { get; set; }
        public byte[] CreatedAt { get; set; }
        public byte[] UpdatedAt { get; set; }
        public string SourceGuildId { get; set; }
        public string SerializedSourceGuild { get; set; }

        public virtual User Creator { get; set; }
        public virtual Guild SourceGuild { get; set; }
        public virtual ICollection<Guild> Guilds { get; set; }
    }
}
