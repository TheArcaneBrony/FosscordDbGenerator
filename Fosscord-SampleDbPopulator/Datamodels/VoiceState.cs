﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class VoiceState
    {
        public string Id { get; set; }
        public string GuildId { get; set; }
        public string ChannelId { get; set; }
        public string UserId { get; set; }
        public string SessionId { get; set; }
        public string Token { get; set; }
        public byte[] Deaf { get; set; }
        public byte[] Mute { get; set; }
        public byte[] SelfDeaf { get; set; }
        public byte[] SelfMute { get; set; }
        public byte[] SelfStream { get; set; }
        public byte[] SelfVideo { get; set; }
        public byte[] Suppress { get; set; }
        public byte[] RequestToSpeakTimestamp { get; set; }

        public virtual Channel Channel { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual User User { get; set; }
    }
}
