﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Webhook
    {
        public Webhook()
        {
            Messages = new HashSet<Message>();
        }

        public string Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Token { get; set; }
        public string GuildId { get; set; }
        public string ChannelId { get; set; }
        public string ApplicationId { get; set; }
        public string UserId { get; set; }
        public string SourceGuildId { get; set; }

        public virtual Application Application { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual Guild SourceGuild { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
    }
}
