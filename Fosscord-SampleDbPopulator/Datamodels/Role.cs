﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Role
    {
        public Role()
        {
            MemberRoles = new HashSet<MemberRole>();
            MessageRoleMentions = new HashSet<MessageRoleMention>();
        }

        public string Id { get; set; }
        public string GuildId { get; set; }
        public long Color { get; set; }
        public byte[] Hoist { get; set; }
        public byte[] Managed { get; set; }
        public byte[] Mentionable { get; set; }
        public string Name { get; set; }
        public string Permissions { get; set; }
        public long Position { get; set; }
        public string Tags { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual ICollection<MemberRole> MemberRoles { get; set; }
        public virtual ICollection<MessageRoleMention> MessageRoleMentions { get; set; }
    }
}
