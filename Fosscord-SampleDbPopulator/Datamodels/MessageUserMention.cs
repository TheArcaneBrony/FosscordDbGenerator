﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class MessageUserMention
    {
        public string MessagesId { get; set; }
        public string UsersId { get; set; }

        public virtual Message Messages { get; set; }
        public virtual User Users { get; set; }
    }
}
