﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class MessageRoleMention
    {
        public string MessagesId { get; set; }
        public string RolesId { get; set; }

        public virtual Message Messages { get; set; }
        public virtual Role Roles { get; set; }
    }
}
