﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class Channel
    {
        public Channel()
        {
            GuildAfkChannels = new HashSet<Guild>();
            GuildPublicUpdatesChannels = new HashSet<Guild>();
            GuildRulesChannels = new HashSet<Guild>();
            GuildSystemChannels = new HashSet<Guild>();
            GuildWidgetChannels = new HashSet<Guild>();
            InverseParent = new HashSet<Channel>();
            Invites = new HashSet<Invite>();
            MessageChannelMentions = new HashSet<MessageChannelMention>();
            Messages = new HashSet<Message>();
            ReadStates = new HashSet<ReadState>();
            Recipients = new HashSet<Recipient>();
            VoiceStates = new HashSet<VoiceState>();
            Webhooks = new HashSet<Webhook>();
        }

        public string Id { get; set; }
        public byte[] CreatedAt { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Type { get; set; }
        public string LastMessageId { get; set; }
        public string GuildId { get; set; }
        public string ParentId { get; set; }
        public string OwnerId { get; set; }
        public long? LastPinTimestamp { get; set; }
        public long? DefaultAutoArchiveDuration { get; set; }
        public long? Position { get; set; }
        public string PermissionOverwrites { get; set; }
        public long? VideoQualityMode { get; set; }
        public long? Bitrate { get; set; }
        public long? UserLimit { get; set; }
        public byte[] Nsfw { get; set; }
        public long? RateLimitPerUser { get; set; }
        public string Topic { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual User Owner { get; set; }
        public virtual Channel Parent { get; set; }
        public virtual ICollection<Guild> GuildAfkChannels { get; set; }
        public virtual ICollection<Guild> GuildPublicUpdatesChannels { get; set; }
        public virtual ICollection<Guild> GuildRulesChannels { get; set; }
        public virtual ICollection<Guild> GuildSystemChannels { get; set; }
        public virtual ICollection<Guild> GuildWidgetChannels { get; set; }
        public virtual ICollection<Channel> InverseParent { get; set; }
        public virtual ICollection<Invite> Invites { get; set; }
        public virtual ICollection<MessageChannelMention> MessageChannelMentions { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<ReadState> ReadStates { get; set; }
        public virtual ICollection<Recipient> Recipients { get; set; }
        public virtual ICollection<VoiceState> VoiceStates { get; set; }
        public virtual ICollection<Webhook> Webhooks { get; set; }
    }
}
