﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Fosscord_SampleDbPopulator.Datamodels
{
    public partial class AuditLog
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ActionType { get; set; }
        public string Options { get; set; }
        public string Changes { get; set; }
        public string Reason { get; set; }
        public string TargetId { get; set; }

        public virtual User Target { get; set; }
        public virtual User User { get; set; }
    }
}
