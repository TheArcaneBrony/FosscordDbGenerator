﻿using System;
using System.Linq;
using Fosscord_SampleDbPopulator.Datamodels;
using RandomNameGeneratorLibrary;

namespace Fosscord_SampleDbPopulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Db db = new();
            Random rnd = new Random();
            PersonNameGenerator nameGen = new PersonNameGenerator();
            int c = 0;
            
            var a = db.Users.Select(x => x.Id);
            ulong highestUID = a.AsEnumerable().Max(x => ulong.Parse(x));

            byte[] f = new[] { (byte)0 };
            byte[] t = new[] { (byte)1 };
            for (int i = 1; i < 100; i++)
            {
                string username = nameGen.GenerateRandomFirstName() + " " + nameGen.GenerateRandomLastName();
                db.Users.Add(new User()
                {
                    Username = username,
                    Avatar = $"https://eu.ui-avatars.com/api/?background=random&name={username.Replace(" ", "+")}",
                    Id = highestUID + (ulong)i + "",
                    Discriminator = rnd.Next(1,10000) + "",
                    Desktop = f,
                    Mobile = f,
                    Bio = "",
                    Premium = t,
                    PremiumType = 2,
                    Bot = f,
                    System = f,
                    NsfwAllowed = t,
                    MfaEnabled = f,
                    CreatedAt = BitConverter.GetBytes(DateTime.Now.Ticks),
                    Verified = t,
                    Disabled = f,
                    Deleted = rnd.Next(2) == 0 ? f : t,
                    Email = $"{username.Replace(" ", ".")}@null.lol",
                    Flags = "0",
                    PublicFlags = 0,
                    Rights = "0",
                    Data = "{\"hash\":\"$2b$12$csWNg0TK5SQkdeOCMcclBuKadTC9c4hakOs7kPNJ1M7PYUU4hjZIG\",\"valid_tokens_since\":\"2021-10-07T18:21:47.962Z\"}",
                    Settings = "{\"timezone_offset\":-120}",
                    Fingerprints = ""
                });
            }

            db.SaveChanges();
            Console.WriteLine("Generated users");
            a = db.Guilds.Select(x => x.Id);
            ulong highestGID = a.AsEnumerable().Max(x => ulong.Parse(x));
            int ucount = db.Users.Count();
            var usrs = db.Users.ToArray();
            for (int i = 1; i < 40; i++)
            {
                db.Guilds.Add(new Guild()
                {
                    Id = highestGID + (ulong)++c + "",
                    Features = "",
                    Name = nameGen.GenerateRandomFirstName(),
                    WelcomeScreen =
                        "{\"enabled\":false,\"description\":\"No description\",\"welcome_channels\":[]}",
                    OwnerId = usrs[rnd.Next(ucount)].Id,

                });
            }
            
            db.SaveChanges();
            db.Dispose();
            db = new Db();
            Console.WriteLine("Generated guilds");

            a = db.Guilds.Select(x => x.Id);
            ulong highestRID = a.AsEnumerable().Max(x => ulong.Parse(x));
            foreach (Guild guild in db.Guilds.Where(x => !db.Roles.Any(y => y.Id == x.Id)))
            {
                db.Roles.Add(new Role()
                {
                    Id = guild.Id,
                    GuildId = guild.Id,
                    Name = "@everyone",
                    Color = 0,
                    Hoist = f,
                    Managed = f,
                    Mentionable = f,
                    Permissions = "2251804225",
                    Position = 0
                });
            }

            db.SaveChanges();
            db.Dispose();
            db = new();
            Console.WriteLine("Generated @everyone roles");
            
            a = db.Channels.Select(x => x.Id);
            ulong highestCID = a.AsEnumerable().Max(x => ulong.Parse(x));
            c = 0;
            foreach (Guild guild in db.Guilds)
            {
                db.Channels.Add(new Channel()
                {
                    Id =highestCID+ (ulong)++c + "",
                    GuildId = guild.Id,
                    CreatedAt = BitConverter.GetBytes(RandomDayFunc().Ticks),
                    Type = "0",
                    Position = 0,
                    Name = nameGen.GenerateRandomFirstName(),
                    
                });
            }

            db.SaveChanges();
            Console.WriteLine("Generated channels");

            a = db.Messages.Select(x => x.Id);
            ulong highestMID = a.AsEnumerable().Max(x => ulong.Parse(x));
            c = 0;
            foreach (Channel channel in db.Channels)
            {
                for (int i = 0; i < 1000; i++)
                {
                    db.Messages.Add(new Message()
                    {
                        Id = highestCID + (ulong) ++c + "",
                        ChannelId =channel.Id,
                        GuildId = channel.GuildId,
                        Timestamp = BitConverter.GetBytes(RandomDayFunc().Ticks),
                        Embeds = "[]",
                        Content = channel.Name,
                        Tts = f,
                        MentionEveryone = f,
                        Reactions = "[]",
                        Type = "0"
                    });    
                }
                db.SaveChanges();
            }

            db.SaveChanges();
            Console.WriteLine("Added messages");
            
            c = db.Members.Count();
            int gc = db.Guilds.Count(), uc = db.Users.Count(), tc = gc * uc;
            foreach (Guild guild in db.Guilds.Where(x=>x.Members.Count() < uc))
            {
                foreach (User user in db.Users.Where(y=>!db.Members.Any(x=>x.GuildId == guild.Id && x.Id == y.Id)))
                {
                    db.Members.Add(new Member()
                    {
                        Index = (long) ++c,
                        Id = user.Id,
                        GuildId = guild.Id,
                        JoinedAt = BitConverter.GetBytes(RandomDayFunc().Ticks),
                        Deaf = f,
                        Mute =f ,
                        Pending = f,
                        Settings = "{\"channel_overrides\":[],\"message_notifications\":0,\"mobile_push\":true,\"muted\":false,\"suppress_everyone\":false,\"suppress_roles\":false,\"version\":0}"
                    });
                }

                
                db.SaveChanges();
                Console.WriteLine($"{c}/{tc} ({c/(double)(tc)*100}%)");
            }

            db.SaveChanges();
            db.Dispose();
            db = new Db();
            Console.WriteLine("Added everyone to every server");
            
            
            Console.WriteLine("Finished!");
        }
        static DateTime RandomDayFunc()
        {
            DateTime start = new DateTime(1995, 1, 1); 
            Random gen = new Random(); 
            int range = ((TimeSpan)(DateTime.Today - start)).Days; 
            return start.AddDays(gen.Next(range));
        }
    }
}